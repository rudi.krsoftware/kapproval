using System;
using System.ComponentModel.DataAnnotations;

namespace K.Approval.Api.DataContext
{
    public class UserLog
    {
        [Key]
        public Guid Id { get; set; }
        [Required]
        public byte RowStatus { get; set; }
        [Required]
        public byte[] RowVersion { get; set; }
        [Required]
        public Guid UserProfileId { get; set; }
        [StringLength(24)]
        public string Action { get; set; }
        public DateTime LogDate { get; set; }
        [StringLength(32)]
        public string ApplicationName { get; set; }
        public string Description { get; set; }
        [Required]
        [StringLength(24)]
        public string CreatedBy { get; set; }
        [Required]
        public DateTime CreatedDate { get; set; }
        [StringLength(24)]
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
    
        public virtual UserProfile UserProfile { get; set; }


    }
}