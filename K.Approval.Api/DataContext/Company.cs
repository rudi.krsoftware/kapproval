using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace K.Approval.Api.DataContext
{
    public class Company
    {
        public Company()
        {
            UserProfiles = new HashSet<UserProfile>();
        }
    
        [Key]
        public Guid Id { get; set; }
        [Required]
        public byte RowStatus { get; set; }
        [Required]
        public byte[] RowVersion { get; set; }
        [Required]
        [StringLength(24)]
        public string CompanyCode { get; set; }
        [Required]
        [StringLength(128)]
        public string CompanyName { get; set; }
        [Required]
        [StringLength(128)]
        public string LegalName { get; set; }
        [StringLength(24)]
        public string BusinessPhone { get; set; }
        [StringLength(24)]
        public string BusinessFax { get; set; }
        [StringLength(128)]
        public string Email { get; set; }
        [StringLength(128)]
        public string Url { get; set; }
        [StringLength(32)]
        public string Npwp { get; set; }
        [StringLength(64)]
        public string Address1 { get; set; }
        [StringLength(64)]
        public string Address2 { get; set; }
        [StringLength(6)]
        public string ZipCode { get; set; }
        [StringLength(64)]
        public string City { get; set; }
        [StringLength(64)]
        public string State { get; set; }
        [StringLength(64)]
        public string Country { get; set; }
        public string Notes { get; set; }
        [Required]
        [StringLength(24)]
        public string CreatedBy { get; set; }
        [Required]
        public DateTime CreatedDate { get; set; }
        [StringLength(24)]
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public virtual ICollection<UserProfile> UserProfiles { get; set; }

    }
}