using System;
using System.ComponentModel.DataAnnotations;

namespace K.Approval.Api.DataContext
{
    public class ProcessActivityActor
    {
        [Key]
        public Guid Id { get; set; }
        [Required]
        public byte RowStatus { get; set; }
        [Required]
        public byte[] RowVersion { get; set; }
        [Required]
        public Guid ProcessActivityId { get; set; }
        [Required]
        [StringLength(24)]
        public string ActorCode { get; set; }
        [Required]
        [StringLength(64)]
        public string ActorName { get; set; }
        [StringLength(64)]
        public string ActorPosition { get; set; }
        [Required]
        public byte ActionType { get; set; }
        [StringLength(256)]
        public string ActorEmail { get; set; }
        [Required]
        [StringLength(24)]
        public string CreatedBy { get; set; }
        [Required]
        public DateTime CreatedDate { get; set; }
        [StringLength(24)]
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
    
        public virtual ProcessActivity ProcessActivity { get; set; }
    }
}