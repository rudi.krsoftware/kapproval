namespace K.Approval.Api.DataContext
{
    public enum DocumentPatternType
    {
        Year = 1,
        Month = 2,
        FixedValue = 3,
        RunningNumber = 99
    }
}