using System;
using System.ComponentModel.DataAnnotations;

namespace K.Approval.Api.DataContext
{
    public class DocumentNumberLog
    {
        [Key]
        public Guid Id { get; set; }
        [Required]
        public byte RowStatus { get; set; }
        [Required]
        public byte[] RowVersion { get; set; }
        [Required]
        public Guid DocumentNumberPatternId { get; set; }
        [Required]
        public int YearNumber { get; set; }
        [Required]
        public int MonthNumber { get; set; }
        [Required]
        public int LastIndex { get; set; }
        [Required]
        [StringLength(24)]
        public string CreatedBy { get; set; }
        [Required]
        public DateTime CreatedDate { get; set; }
        [StringLength(24)]
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public virtual DocumentNumberPattern DocumentNumberPattern { get; set; }
    }
}