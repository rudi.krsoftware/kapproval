using System;

namespace K.Approval.Api.Common
{
    public enum RequestAction
    {
        Submit,
        Approve,
        Revise,
        Reject,
        Postpone,
        Closed,
        ReSubmit
    }

    /*class RequestActionToString
    {
        public static string ToString(RequestAction action)
        {
            switch (action)
            {
                case RequestAction.Submit:
                    return "Pengajuan";
                case RequestAction.Approve:
                    return "Disetujui";
                case RequestAction.Revise:
                    return "Koreksi";
                case RequestAction.Reject:
                    return "Ditolak";
                case RequestAction.Postpone:
                    return "Ditahan";
                case RequestAction.Closed:
                    return "Ditutup";
                case RequestAction.ReSubmit:
                    return "Pengajuan Ulang";
                default:
                    return "";
            }
        }
    }*/
}