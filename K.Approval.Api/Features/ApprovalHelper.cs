using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using K.Approval.Api.Common;
using K.Approval.Api.DataContext;
using K.Approval.Api.Services;
using KCore.Common.Extensions;
using KCore.Common.Fault;
using Microsoft.EntityFrameworkCore;
using Omu.ValueInjecter;

namespace K.Approval.Api.Features
{
    internal sealed class ApprovalHelper
    {
        internal string DisplayStatusNextActivity { get; set; }
        public event EventHandler<ProcessEventArgs> NextActivityHasChangedEvent;
        private readonly WorkflowDataContext _workflowDataContext;
        public ApprovalHelper(WorkflowDataContext workflowDataContext)
        {
            _workflowDataContext = workflowDataContext;
        }
        
        private static string SetDisplayName(ProcessActivity nextActivityRequest, RequestParameter requestParameter)
        {
            switch (requestParameter.ActionName)
            {
                case RequestAction.Reject:
                    return RequestStatusEnum.Rejected.ToString().ToUpper();
                case RequestAction.Revise:
                    return RequestStatusEnum.Revised.ToString().ToUpper();
                default:
                    return nextActivityRequest.DisplayName;
            }
        }
        
        public static string SetDisplayName(ProcessActivity nextActivityRequest, RequestAction action)
        {
            switch (action)
            {
                case RequestAction.Reject:
                    return RequestStatusEnum.Rejected.ToString().ToUpper();
                case RequestAction.Revise:
                    return RequestStatusEnum.Revised.ToString().ToUpper();
                default:
                    return nextActivityRequest.DisplayName;
            }
        }

        public static string SetRequestStatus(ProcessActivity nextActivityRequest, RequestParameter requestParameter)
        {
            switch (requestParameter.ActionName)
            {
                case RequestAction.Reject:
                    return RequestStatusEnum.Rejected.ToString().ToUpper();
                case RequestAction.Revise:
                    return RequestStatusEnum.Revised.ToString().ToUpper();
                default:
                    return nextActivityRequest.NewStatus.ToUpper();
            }
        }
        
        public static string SetRequestStatus(ProcessActivity nextActivityRequest, RequestAction action)
        {
            switch (action)
            {
                case RequestAction.Reject:
                    return RequestStatusEnum.Rejected.ToString().ToUpper();
                case RequestAction.Revise:
                    return RequestStatusEnum.Revised.ToString().ToUpper();
                default:
                    return nextActivityRequest.NewStatus.ToUpper();
            }
        }
        
        public static string CreatePostSubject(RequestActivity activityRequest, RequestStatus requestStatus, string requestFullName, string requestAction, string documentNumber)
        {
            return activityRequest.SubjectName.CreateSubject(requestStatus.RequestNumber, requestFullName,requestAction, documentNumber);
        }
        
        public async Task ApprovalProcess(ProcessActivity activityRequest, RequestParameter requestParameter)
        {

            var nextActivityRequest = await _workflowDataContext.ProcessActivities.FirstOrDefaultAsync(
                c =>
                    c.RowStatus == 0 && c.ProcessRequestId == activityRequest.ProcessRequestId &&
                    c.ActivityIndex == activityRequest.ActivityIndex + 1);

            if (nextActivityRequest == null) throw new ApiException("Tidak ada data Activity Request selanjutnya", HttpStatusCode.BadRequest);
            var requestStatus =
                await _workflowDataContext.RequestStatuses.FirstOrDefaultAsync(
                    c => c.RowStatus == 0 && c.ObjectKey == requestParameter.ObjectKey && c.RequestNumber == requestParameter.RequestNumber);
            if (requestStatus == null) throw new Exception("Tidak ada data Request Status");
            var displayStatus = SetDisplayName(nextActivityRequest, requestParameter);
            var newRequestStatus = SetRequestStatus(nextActivityRequest, requestParameter);
            
            requestStatus.ProcessActivityId = nextActivityRequest.Id;
            requestStatus.LastAssignDate = requestParameter.RequestDate;
            requestStatus.LastAssignTo = requestParameter.FullName;
            requestStatus.DisplayStatus = displayStatus;
            requestStatus.NewRequestStatus = newRequestStatus;
            requestStatus.ModifiedBy = requestParameter.UserName;
            requestStatus.ModifiedDate = DateTime.Now;  //requestParameter.RequestDate;
            requestStatus.Notes = "";//requestParameter.Description;

            requestStatus.Subject =
                CreatePreSubject(nextActivityRequest, requestStatus, requestParameter.FullName, requestParameter); //nextActivityRequest.SubjectName;
            requestStatus.SlaTime = nextActivityRequest.SlaTime;
            requestStatus.SlaType = nextActivityRequest.SlaType;
            requestStatus.DocumentName = requestParameter.DocumentName;
            requestStatus.DocumentNumber = requestParameter.DocumentNumber;
            requestStatus.RequestNumber = requestParameter.RequestNumber;

            if (nextActivityRequest.NewStatus.Compare(RequestStatusEnum.Closed))
            {
                requestStatus.IsComplete = 1;
                requestStatus.CompleteDate = DateTime.Now;
            }
            else
            {
                requestStatus.IsComplete = 0;
                requestStatus.CompleteDate = null;
            }

            if (nextActivityRequest.NewStatus.Compare(RequestStatusEnum.Closed))
            {
                var entity = new RequestActivity
                {
                    Description = "", //requestParameter.Description,
                    RequestStatus = nextActivityRequest.NewStatus,
                    ProcessActivityId = nextActivityRequest.Id,
                    ActivityIndex = nextActivityRequest.ActivityIndex,
                    DisplayStatus = nextActivityRequest.DisplayName,
                    ActorCode = requestParameter.UserName,
                    ActionDate = DateTime.Now,
                    ActorName = requestParameter.FullName,
                    ActionName = SetActionName(nextActivityRequest, requestParameter),
                    CreatedBy = requestParameter.UserName,
                    CreatedDate = DateTime.Now,
                    Id = Guid.NewGuid(),
                    IsComplete = 1,
                    ModifiedBy = requestParameter.UserName,
                    ModifiedDate = DateTime.Now,
                    RowStatus = 0,
                    SlaTime = null,
                    SlaType = null,
                    SubjectName = CreatePreSubject(nextActivityRequest, requestStatus, requestParameter.FullName, requestParameter),
                    ObjectKey = requestParameter.ObjectKey,
                    RequestNumber = requestParameter.RequestNumber,
                    DocumentName = requestParameter.DocumentName,
                    DocumentNumber = requestParameter.DocumentNumber,
                };
                await _workflowDataContext.RequestActivities.AddAsync(entity);
            }

            var submitterActors = await GetSubmitter(requestParameter.RequestNumber);
            var activityActors = await _workflowDataContext.ProcessActivityActors.Where(c => c.ProcessActivityId == nextActivityRequest.Id).ToListAsync();
            var emailTo = "";
            foreach (var activityActor in activityActors)
            {
                if (submitterActors.Any(x => x.ActorCode == activityActor.ActorCode))
                {
                    var requestInbox = new RequestInbox
                    {
                        Id = Guid.NewGuid(),
                        CreatedDate = DateTime.Now,
                        CreatedBy = requestParameter.UserName,
                        RowStatus = 0,
                        ModifiedBy = string.Empty,
                        ModifiedDate = DateTime.Now,
                        CommitmentDate = null,
                        RequestDate = requestStatus.RequestDate,
                        ObjectId = requestStatus.ObjectKey,
                        RequestNumber = requestParameter.RequestNumber,
                        DisplayStatus = displayStatus,
                        RequestStatus = newRequestStatus,
                        ActorNameRequester = requestParameter.FullName,
                        Subject = CreatePreSubjectForSubmitter(nextActivityRequest, requestStatus,
                            requestStatus.ActorName,
                            requestParameter),
                        ActivityUrl = "",
                        AssignDate = DateTime.Now,
                        CompleteDate = GetCompleteDate(nextActivityRequest, requestParameter),
                        HasView = false,
                        IsComplete = GetIsComplete(nextActivityRequest, requestParameter),
                        IsDelegatee = false,
                        RequestDelegateFromId = string.Empty,
                        RequestDelegateFromName = string.Empty,
                        ActorCodeAssignee = requestStatus.ActorCode,
                        ActorNameAssignee = requestStatus.ActorName,
                        ActorCodeRequester = requestParameter.UserName,
                        JavascriptAction = nextActivityRequest.ViewJavascriptAction,
                        UrlAction = nextActivityRequest.UrlAction,
                        UrlActionType = nextActivityRequest.UrlActionType,
                        ProcessActivityId = nextActivityRequest.Id,
                        Description = requestParameter.Description,
                        ActionType = GetInboxActionType(InboxActionType.Initiator),
                        DocumentName = requestParameter.DocumentName,
                        DocumentNumber = requestParameter.DocumentNumber,
                    };
                    await _workflowDataContext.RequestInboxes.AddAsync(requestInbox);
                }
                else
                {
                    var requestInbox1 = new RequestInbox
                    {
                        Id = Guid.NewGuid(),
                        CreatedDate = DateTime.Now,
                        CreatedBy = requestParameter.UserName,
                        RowStatus = 0,
                        ModifiedBy = string.Empty,
                        ModifiedDate = DateTime.Now,
                        CommitmentDate = null,
                        RequestDate = requestStatus.RequestDate,
                        ObjectId = requestStatus.ObjectKey,
                        RequestNumber = requestParameter.RequestNumber,
                        DisplayStatus = displayStatus,
                        RequestStatus = newRequestStatus,
                        ActorNameRequester = requestStatus.ActorName,
                        Subject = CreatePreSubject(nextActivityRequest, requestStatus, activityActor, requestParameter),
                        ActivityUrl = "",
                        AssignDate = DateTime.Now,
                        CompleteDate = GetCompleteDate(nextActivityRequest, requestParameter),
                        HasView = false,
                        IsComplete = GetIsComplete(nextActivityRequest, requestParameter),
                        IsDelegatee = false,
                        RequestDelegateFromId = string.Empty,
                        RequestDelegateFromName = string.Empty,
                        ActorCodeAssignee = activityActor.ActorCode,
                        ActorNameAssignee = activityActor.ActorName,
                        ActorCodeRequester = requestStatus.ActorCode,
                        JavascriptAction = activityActor.ActionType == 2
                            ? nextActivityRequest.ApprovalJavascriptAction
                            : nextActivityRequest.ViewJavascriptAction,
                        UrlAction = nextActivityRequest.UrlAction,
                        UrlActionType = nextActivityRequest.UrlActionType,
                        ProcessActivityId = nextActivityRequest.Id,
                        Description = requestParameter.Description,
                        ActionType = GetInboxActionType(activityActor),
                        DocumentName = requestParameter.DocumentName,
                        DocumentNumber = requestParameter.DocumentNumber,
                    };
                    await _workflowDataContext.RequestInboxes.AddAsync(requestInbox1);

                }
                
                // for viewer
                if (activityActor.ActionType != 2) continue;

                requestStatus.LastAssignDate = DateTime.Now;
                requestStatus.LastAssignTo = activityActor.ActorCode;
                requestStatus.ActorCode = activityActor.ActorCode;
                requestStatus.ActorName = activityActor.ActorName;
                
                var newActivity = new RequestActivity
                {
                    Description = "",//requestParameter.Description,
                    RequestStatus = nextActivityRequest.NewStatus,
                    ProcessActivityId = nextActivityRequest.Id,
                    ActivityIndex = nextActivityRequest.ActivityIndex,
                    DisplayStatus = nextActivityRequest.DisplayName,
                    ActorCode = activityActor.ActorCode,
                    ActionDate = null,
                    ActorName = activityActor.ActorName,
                    ActionName = null,
                    CreatedBy = requestParameter.UserName,
                    CreatedDate = DateTime.Now,
                    Id = Guid.NewGuid(),
                    IsComplete = 0,
                    ModifiedBy = requestParameter.UserName,
                    ModifiedDate = DateTime.Now,
                    RowStatus = 0,
                    SlaTime = null,
                    SlaType = null,
                    SubjectName = CreatePreSubject(nextActivityRequest, requestStatus, activityActor, requestParameter),
                    ObjectKey = requestParameter.ObjectKey,
                    RequestNumber = requestParameter.RequestNumber,
                    DocumentName = requestParameter.DocumentName,
                    DocumentNumber = requestParameter.DocumentNumber,
                    
                };
                await _workflowDataContext.RequestActivities.AddAsync(newActivity);
            }

            var emailTask = CreateEmailTask(requestParameter, nextActivityRequest, emailTo);

            await _workflowDataContext.EmailTasks.AddAsync(emailTask);
            DisplayStatusNextActivity = nextActivityRequest.DisplayName;
            NextActivityHasChanged(new ProcessEventArgs
            {
                DisplayStatus = nextActivityRequest.DisplayName,
                NewStatus = nextActivityRequest.NewStatus,
                OldStatus = activityRequest.NewStatus,
                NoAction = false
            });
        }

        private async Task<List<ProcessActivityActor>> GetSubmitter(string requestNumber)
        {
            var processActivity = await _workflowDataContext.ProcessActivities
                .Include("ProcessRequest")
                .Include("ProcessActivityActors")
                .FirstOrDefaultAsync(x => x.ProcessRequest.RequestNumber == requestNumber && x.ActivityIndex == 1);
            return processActivity.ProcessActivityActors.Where(x => x.ActionType == (int) InboxActionType.Initiator)
                .ToList();

        }

        private static string CreatePreSubjectForSubmitter(ProcessActivity nextActivityRequest, RequestStatus requestStatus, string requestStatusActorName, RequestParameter requestParameter)
        {
            return nextActivityRequest.PostSubject.CreateSubject(requestStatus.RequestNumber, requestStatusActorName,
                "", requestParameter.DocumentNumber);
        }

        internal static EmailTask CreateEmailTask(RequestParameter requestParameter, ProcessActivity nextActivityRequest, string emailTo)
        {
            return new EmailTask
            {
                Id = Guid.NewGuid(),
                CreatedDate = DateTime.Now,
                CreatedBy = requestParameter.UserName,
                RowStatus = 0,
                ModifiedBy = "",
                ModifiedDate = null,
                EmailBody = "",
                EmailCc = "",
                EmailFrom = "",
                EmailSubject = nextActivityRequest.SubjectName,
                EmailTo = emailTo,
                SourceId = nextActivityRequest.Id,
                TaskFrom = nextActivityRequest.SubjectName
            };
        }
        
        internal static EmailTask CreateEmailTask(ProcessActivity nextActivityRequest, string emailTo, string employeeCode)
        {
            return new EmailTask
            {
                Id = Guid.NewGuid(),
                CreatedDate = DateTime.Now,
                CreatedBy = employeeCode,
                RowStatus = 0,
                ModifiedBy = "",
                ModifiedDate = null,
                EmailBody = "",
                EmailCc = "",
                EmailFrom = "",
                EmailSubject = nextActivityRequest.SubjectName,
                EmailTo = emailTo,
                SourceId = nextActivityRequest.Id,
                TaskFrom = nextActivityRequest.SubjectName
            };
        }
        
        private static string GetInboxActionType(ProcessActivityActor activityActor)
        {
            return activityActor.ActionType == 2 ? InboxActionType.Approval.ToString() : InboxActionType.View.ToString();
        }
        
        private static string GetInboxActionType(InboxActionType inboxActionType)
        {
            return inboxActionType == InboxActionType.Approval ? InboxActionType.Approval.ToString() : InboxActionType.View.ToString();
        }
        
        private static bool GetIsComplete(ProcessActivity nextActivityRequest, RequestParameter requestParameter)
        {
            if (nextActivityRequest.NewStatus.Compare(RequestStatusEnum.Closed)) return true;
            if (requestParameter.ActionName == RequestAction.Reject) return true;
            return requestParameter.ActionName == RequestAction.Revise;
        }

        private static DateTime? GetCompleteDate(ProcessActivity nextActivityRequest, RequestParameter requestParameter)
        {
            if (nextActivityRequest.NewStatus.Compare(RequestStatusEnum.Closed) || requestParameter.ActionName == RequestAction.Reject || requestParameter.ActionName == RequestAction.Revise)
                return DateTime.Now;
            return null;
        }
        
        internal static string CreatePreSubject(ProcessActivity nextActivityRequest, RequestStatus requestStatus, ProcessActivityActor activityActor, RequestParameter requestParameter)
        {
            if (activityActor.ActionType == 2)
            {
                return nextActivityRequest.SubjectName.CreateSubject(requestStatus.RequestNumber, activityActor.ActorName,
                    "", requestParameter.DocumentNumber);
            }
            return nextActivityRequest.ViewSubject.CreateSubject(requestStatus.RequestNumber, activityActor.ActorName,
                "", requestParameter.DocumentNumber);
        }
        
        internal static string CreatePreSubject(ProcessActivity nextActivityRequest, RequestStatus requestStatus, string userFullName, RequestParameter requestParameter)
        {
            return nextActivityRequest.ViewSubject.CreateSubject(requestStatus.RequestNumber, userFullName,
                "", requestParameter.DocumentNumber);
        }
        
        private static string SetActionName(ProcessActivity nextActivityRequest, RequestParameter requestParameter)
        {
            return requestParameter.ActionName == RequestAction.Reject ? RequestAction.Reject.ToString() : RequestAction.Closed.ToString();
        }

        private void NextActivityHasChanged(ProcessEventArgs e)
        {
            var handler = NextActivityHasChangedEvent;
            handler?.Invoke(this, e);
        }

        public async Task<ProcessRequest> BaseNewProcess(RequestParameter requestParameter)
        {
            var processRequest = await  _workflowDataContext.TProcessRequests.FirstOrDefaultAsync(
                        a => a.ModuleId.ToUpper().Equals(requestParameter.ModuleId.ToUpper()) && a.CompanyId == requestParameter.CompanyId &&
                             a.EffectiveDate <= DateTime.Now.Date);

            if (processRequest == null)
                throw new ApiException(ProcessError.InvalidModule);

            
            var tempProcessActivities =
                await _workflowDataContext.TProcessActivities.Where(c => c.ProcessRequestId.Equals(processRequest.Id)).ToListAsync();
            if (tempProcessActivities.Count == 0)
                throw new ApiException(ProcessError.InvalidConfigurationProcessActivity);


            var tempActivityActors = new List<TProcessActivityActor>();
            foreach (var actors in tempProcessActivities.Select(x => _workflowDataContext.TProcessActivityActors.Where(c => c.ProcessActivityId.Equals(x.Id)).ToList()))
            {
                tempActivityActors.AddRange(actors);
            }


            var newProcessRequest = new ProcessRequest();
            newProcessRequest.InjectFrom(processRequest);

            var newProcessActivities = new List<ProcessActivity>();
            newProcessActivities.AddRange(
                tempProcessActivities.Select(source => (ProcessActivity) new ProcessActivity().InjectFrom(source)));

            var newProcessActivityActors = new List<ProcessActivityActor>();
            newProcessActivityActors.AddRange(
                tempActivityActors.Select(source => (ProcessActivityActor)new ProcessActivityActor().InjectFrom(source)));



            newProcessRequest.Id = Guid.NewGuid();
            newProcessRequest.CreatedBy = requestParameter.UserName;
            newProcessRequest.CreatedDate = DateTime.UtcNow;
            await _workflowDataContext.ProcessRequests.AddAsync(newProcessRequest);

            foreach (var entity in newProcessActivities)
            {
                var newGuid = Guid.NewGuid();
                foreach (var actor in newProcessActivityActors.Where(x => x.ProcessActivityId == entity.Id))
                {
                    actor.Id = Guid.NewGuid();
                    actor.ProcessActivityId = newGuid;
                    actor.CreatedBy = requestParameter.UserName;
                    actor.CreatedDate = DateTime.UtcNow;
                }
                entity.Id = newGuid;
                entity.ProcessRequestId = newProcessRequest.Id;
                entity.CreatedBy = requestParameter.UserName;
                entity.CreatedDate = DateTime.UtcNow;
            }
            
            var service = new DocumentNumberService(_workflowDataContext);
            var requestNumber = await service.GetNewNumber("REQUEST_NUMBER");
            newProcessRequest.RequestNumber = requestNumber;
            newProcessRequest.CreatedBy = requestParameter.UserName;
            newProcessRequest.CreatedDate = DateTime.UtcNow;

            await _workflowDataContext.ProcessActivities.AddRangeAsync(newProcessActivities);
            await _workflowDataContext.ProcessActivityActors.AddRangeAsync(newProcessActivityActors);
            await _workflowDataContext.SaveChangesAsync();

            var activeRequest  = await
                _workflowDataContext.ProcessRequests.FirstOrDefaultAsync(
                    c => c.RowStatus == 0 && c.RequestNumber == requestNumber);
            
            if (activeRequest == null) throw new ApiException(ProcessError.InvalidRequestNumber(requestNumber));

            return activeRequest;
        }

        public static string CreateRejectMessage(ProcessActivity processActivity, RequestActivity requestStatus, string action, string requestFullName)
        {
            return processActivity.RejectMessage.CreateSubject(requestStatus.RequestNumber, requestFullName, action, requestStatus.DocumentNumber);
        }
        
        public static string CreateReviseMessage(ProcessActivity processActivity, RequestActivity requestStatus, string action, string requestFullName)
        {
            return processActivity.ReviseMessage.CreateSubject(requestStatus.RequestNumber, requestFullName, action, requestStatus.DocumentNumber);
        }
    }
}