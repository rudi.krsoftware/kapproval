using System.Collections.Generic;
using K.Approval.Api.Common;
using KCore.Common.Data;
using KCore.Common.Response;
using MediatR;

namespace K.Approval.Api.Features.GetInbox
{
    public class Request: BaseRequest, IRequest<ApiResult<Response>>
    {
        public Paging Paging { get; set; }
        public IEnumerable<Sort> Sorts { get; set; }
        public IEnumerable<IParameter> ListParameters { get; set; }
        public string Email { get; set; }
    }
}