using System;
using System.Threading.Tasks;
using K.Approval.Api.Common;
using K.Approval.Api.DataContext;
using K.Approval.Api.Infrastructures;
using KCore.Common.Fault;
using Microsoft.EntityFrameworkCore;

namespace K.Approval.Api.Features.Approval
{
    public class Validation : RequestValidator<Request>
    {
        private readonly WorkflowDataContext _workflowDataContext;
        public Validation(WorkflowDataContext workflowDataContext)
        {
            _workflowDataContext = workflowDataContext;
        }

        public override async Task<ValidationResult> InternalValidate(Request request)
        {
            if (request == null) return ValidationResult.ValidationError(DefaultError.InvalidRequest);
                
            var applicationRegistration = await _workflowDataContext.ApplicationRegistrations.Include(x=>x.Company)
                .FirstOrDefaultAsync(x => x.ApplicationSecret == request.AuthKey && x.RowStatus == 0);

            if (applicationRegistration?.Company == null || applicationRegistration.Company.RowStatus != 0)
            {
                return ValidationResult.ValidationError(DefaultError.InvalidApplicationRegistration);
            }
            
            if (!string.Equals(request.Action, RequestAction.Approve.ToString(), StringComparison.CurrentCultureIgnoreCase))
                return ValidationResult.ValidationError(ProcessError.InvalidRequestAction);

            var userProfile = await _workflowDataContext.UserProfiles.FirstOrDefaultAsync(x => x.Email == request.Email && 
                                                                                               x.RowStatus == 0 && x.CompanyId == applicationRegistration.CompanyId);
            if (userProfile == null)
                return ValidationResult.ValidationError(DefaultError.InvalidUser);
            
            request.Company = applicationRegistration.Company;
            request.UserProfile = userProfile;
            return ValidationResult.Ok();
        }
    }
}