using System;
using K.Approval.Api.DataContext;
using KCore.Common.Test;

namespace K.Approval.Api.Test.Builders
{
    public class TProcessActivityActorBuilder : BaseFluentBuilder<TProcessActivityActor>
    {
        public TProcessActivityActorBuilder()
        {
            SetProperty(x => x.Id, Guid.NewGuid());
            SetProperty(x => x.RowStatus, RowStatus);
        }
        
        public TProcessActivityActorBuilder WithProcessActivity(Guid data)
        {
            SetProperty(x => x.ProcessActivityId, data);
            return this;
        }
        
        public TProcessActivityActorBuilder WithActor(string code, string name)
        {
            SetProperty(x => x.ActorCode, code);
            SetProperty(x => x.ActorName, name);
            return this;
        }
        
        public TProcessActivityActorBuilder WithActionType(byte data)
        {
            SetProperty(x => x.ActionType, data);
            return this;
        }
    }
}